#!/usr/bin/env bash

printLine () {
    echo "================================";
    echo "$1"
    echo "================================";
}

printLine "Crizza Ubuntu - Utilities Setup Started"

###############################################################################
echo "Updating package list..."
###############################################################################
export DEBIAN_FRONTEND=noninteractive

apt-get	install -y apt-utils
apt-get install -y software-properties-common
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C
add-apt-repository ppa:ondrej/php -y
apt-get install -y tzdata
ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

#############################################################################
echo "Setting up users and utilities..."
#############################################################################
apt-get -y install curl
apt-get -y install nano
apt-get -y install bash-completion
apt-get -y install sudo
apt-get -y install zip
apt-get -y install unzip
apt-get -y install net-tools
apt-get -y install dnsutils

DOCKER_USER_DIR='/home/'$DOCKER_USER

useradd -m $DOCKER_USER
adduser $DOCKER_USER sudo

echo "$DOCKER_USER:$DOCKER_GROUP" | chpasswd
echo "$DOCKER_USER ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

PROMPT="\"\[\e[1;32m\]\u@\h\[\e[m\]:\[\033[01;34m\][\w]\[\033[00m\] \""
echo "export PS1=$PROMPT " >> $DOCKER_USER_DIR/.bashrc;

##############################################################################
echo "Installing VCS Clients..."
##############################################################################
apt-get -y install git

##############################################################################
echo "Installing PHP and modules..."
##############################################################################

for VERSION in 7.4 8.1
do
	apt-get -y install php${VERSION}-fpm
	apt-get -y install php${VERSION}-xml
	apt-get -y install php${VERSION}-zip
	apt-get -y install php${VERSION}-xdebug
	apt-get -y install php${VERSION}-intl
	apt-get -y install php${VERSION}-mysql
	apt-get -y install php${VERSION}-curl
	apt-get -y install php${VERSION}-mbstring
	apt-get -y install php${VERSION}-gd
	apt-get -y install php${VERSION}-soap
	apt-get -y install php${VERSION}-bcmath
	apt-get -y install php${VERSION}-mbstring
	apt-get -y install php${VERSION}-apcu
	apt-get -y install php${VERSION}-common
	apt-get -y install php${VERSION}-cli
	apt-get -y install php${VERSION}-imap
	apt-get -y install php${VERSION}-json
	apt-get -y install php${VERSION}-opcache
	apt-get -y install php${VERSION}-readline
done

phpdismod xdebug

#Clean apt
rm -rf /var/lib/apt/lists/*

printLine "Crizza Ubuntu - Utilities Setup Completed"
