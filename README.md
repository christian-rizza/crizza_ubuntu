## PHP-FPM Ubuntu based container

This is a Dockerfile/image to build a container with php-fpm.
It contains multiple version of php and php-fpm

*   PHP 7.4
*   PHP 8.1

for each of them the following modules come pre-installed as well:

*   php-xml
*   php-zip
*   php-xdebug
*   php-intl
*   php-mysql
*   php-curl
*   php-mbstring
*   php-gd
*   php-soap
*   php-bcmath

### Quick Start

To pull from docker hub:

`docker pull crizza/ubuntu`

### Running

Run php 7.4:

`docker run crizza/ubuntu php7.4`

`docker run -it crizza/ubuntu php7.4 -a`

Run php 8.1:

`docker run crizza/ubuntu php8.1`

`docker run -it crizza/ubuntu php8.1 -a`